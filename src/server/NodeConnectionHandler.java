package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import server.packet.PacketBase;
import server.packet.control.AskJoinNetwork;
import server.packet.control.ValidateAskJoinNetwork;
import util.Utils;

public class NodeConnectionHandler extends Thread {

    public final Server the_server_;

    public final Socket my_socket_;

    public final ObjectInputStream my_socket_data_in_;

    public final ObjectOutputStream my_socket_data_out_;

    protected boolean do_initiate_connection_;

    /**
     * Not final because we wont set this member in th constructor.
     */
    public Node my_destination_node_info_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // NodeConnectionHandler implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * @param the_server
     * @param the_node_info
     * @throws NumberFormatException
     * @throws UnknownHostException
     * @throws IOException
     */
    public NodeConnectionHandler(Server the_server, String the_addr, String the_port) throws NumberFormatException,
                                                                                      UnknownHostException,
                                                                                      IOException {
        this(the_server, new Socket(the_addr, Short.parseShort(the_port)), true);
    }

    public NodeConnectionHandler(Server the_server,
                                 Socket my_socket,
                                 boolean do_initiate_connection) throws IOException {
        the_server_ = the_server;
        my_socket_ = my_socket;
        my_socket_data_out_ = new ObjectOutputStream(my_socket_.getOutputStream()); // Output first !
        my_socket_data_out_.flush(); // Flush asap for the receiver to be able to call ObjectInputStream without
                                     // blocking.
        my_socket_data_in_ = new ObjectInputStream(my_socket_.getInputStream());
        do_initiate_connection_ = do_initiate_connection;
    }

    @Override
    public void run() {
        try {
            Utils.Log("Handling a new connection");
            if (do_initiate_connection_) {
                Utils.Log("Initiating the new connection");
                new AskJoinNetwork(the_server_.my_node_).ActionOnSend(this);

                // We expect to get a ShareNeighbor packet next. This packet will contain the
                // data about not destination.
                PacketBase the_packet = ((PacketBase) my_socket_data_in_.readObject());
                Utils.BetterAssert(the_packet instanceof ValidateAskJoinNetwork);

                the_packet.ActionOnReceive(this);
            }

            while (my_socket_.isConnected()) {
                PacketBase the_packet = ((PacketBase) my_socket_data_in_.readObject());
                the_packet.ActionOnReceive(this);
            }

        } catch (Exception e) {
            // e.printStackTrace();
        } finally {
            try {
                the_server_.the_neighbor_nodes_mutex_.lock();

                // Garbage collector, this method of removing a NodeConnectionHandler is not
                // always used. Sometime, the Action require the removal of the/a
                // NodeConnectionHandler while executing the packet's Action.
                // Thus, check that we are not deleting an other NodeConnectionHandler with the
                // same guid (it can happen).
                if (my_destination_node_info_ != null
                        && the_server_.the_neighbor_nodes_.containsKey(my_destination_node_info_.GetTheData().the_guid_)) {
                    if (the_server_.the_neighbor_nodes_.get(my_destination_node_info_.GetTheData().the_guid_) == this) { // check
                                                                                                                         // for
                                                                                                                         // same
                                                                                                                         // reference

                        the_server_.the_neighbor_nodes_.remove(my_destination_node_info_.GetTheData().the_guid_);
                    }
                }

                Utils.Log("Closing a socket connected to " + my_socket_.getInetAddress() + ':' + my_socket_.getPort());
                my_socket_.close();
                the_server_.the_neighbor_nodes_mutex_.unlock();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "NodeConnectionHandler [my_destination_node_info_=" + my_destination_node_info_ + "]";
    }

}
