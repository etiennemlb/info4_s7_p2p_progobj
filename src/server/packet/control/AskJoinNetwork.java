package server.packet.control;

import java.io.IOException;

import server.Node;
import server.NodeConnectionHandler;
import server.packet.PacketBase;

public class AskJoinNetwork implements PacketBase {

    /**
     * 
     */
    private static final long serialVersionUID = 4907893431648465306L;

    /**
     * This peer want to enter the network.
     */
    protected final Node the_sender_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // JoinNetwork implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public AskJoinNetwork(Node the_sender) {
        the_sender_ = the_sender;
    }

    /**
     * When a node receive this packet type, it needs to do the following :
     * 
     * - Check if it can accept the connection (less than Server.kMaxNeighborCount
     * neighbors). If the connection can be accepted, the ActionOnReceive adds
     * the_sender_ to the server map (guid->NodeConnectionHandler). Then it sends
     * the list of all known neighbors (itself excluded).
     * 
     * - Else, send a SwitchPeer packet to one of our neighbors.
     */
    @Override
    public void ActionOnReceive(NodeConnectionHandler the_node_handler) {

        the_node_handler.my_destination_node_info_ = the_sender_;

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.lock();

        if (the_node_handler.the_server_.the_neighbor_nodes_.containsKey(the_sender_.GetTheData().the_guid_)) {
            try {
                the_node_handler.my_socket_.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
            return;
        }

        if (!the_node_handler.the_server_.HasFreeNodeSlot()
                && !the_node_handler.the_server_.the_neighbor_nodes_.containsKey(the_sender_.GetTheData().the_guid_)) {

            // first in map ~~ pseudo random not ideal
            NodeConnectionHandler the_switching_node = the_node_handler.the_server_.the_neighbor_nodes_.entrySet()
                                                                                                       .iterator()
                                                                                                       .next()
                                                                                                       .getValue();

            new SwitchPeer(the_sender_).ActionOnSend(the_switching_node);
        }

        the_node_handler.the_server_.the_neighbor_nodes_.put(the_sender_.GetTheData().the_guid_, the_node_handler);

        new ValidateAskJoinNetwork(the_node_handler).ActionOnSend(the_node_handler);

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
    }

    @Override
    public void ActionOnSend(NodeConnectionHandler the_node_handler) {

        try {
            the_node_handler.my_socket_data_out_.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
