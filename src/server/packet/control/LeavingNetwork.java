package server.packet.control;

import java.io.IOException;

import server.NodeConnectionHandler;
import server.packet.PacketBase;
import util.Utils;

public class LeavingNetwork implements PacketBase {

    /**
     * 
     */
    private static final long serialVersionUID = 3799938979772450217L;

    protected final ShareNeighbors the_neighbors_; // composition over inheritance

    //////////////////////////////////////////////////////////////////////////////////////////
    // LeavingNetwork implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public LeavingNetwork(NodeConnectionHandler the_handler_to_exclude) {
        the_neighbors_ = new ShareNeighbors(the_handler_to_exclude);
    }

    @Override
    public void ActionOnReceive(NodeConnectionHandler the_node_handler) {

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.lock();
        the_node_handler.the_server_.the_neighbor_nodes_.remove(the_node_handler.my_destination_node_info_.GetTheData().the_guid_);

        the_neighbors_.ActionOnReceive(the_node_handler);

        try {
            the_node_handler.my_socket_.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
    }

    /**
     * Be sure the_neighbor_nodes_mutex_ is locked ! Also, this method will modify
     * the_neighbor_nodes_
     */
    @Override
    public void ActionOnSend(NodeConnectionHandler the_node_handler) {

        Utils.BetterAssert(the_node_handler.the_server_.the_neighbor_nodes_mutex_.isLocked());

        the_node_handler.the_server_.the_neighbor_nodes_.remove(the_node_handler.my_destination_node_info_.GetTheData().the_guid_);

        try {
            the_node_handler.my_socket_data_out_.writeObject(this);
            the_node_handler.my_socket_.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
