package server.packet.control;

import java.io.IOException;

import server.Node;
import server.NodeConnectionHandler;
import server.packet.PacketBase;

public class ValidateAskJoinNetwork implements PacketBase {

    /**
     * 
     */
    private static final long serialVersionUID = -4883735455067003483L;

    protected final Node the_sender_;
    protected final ShareNeighbors the_neighbors_; // composition over inheritance

    //////////////////////////////////////////////////////////////////////////////////////////
    // ValidateAskJoinNetwork implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public ValidateAskJoinNetwork(NodeConnectionHandler the_handler_to_exclude) {
        the_sender_ = the_handler_to_exclude.the_server_.my_node_;
        the_neighbors_ = new ShareNeighbors(the_handler_to_exclude);
    }

    @Override
    public void ActionOnReceive(NodeConnectionHandler the_node_handler) {

        the_node_handler.my_destination_node_info_ = the_sender_;

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.lock();

        if (!the_node_handler.the_server_.HasFreeNodeSlot()) {
            // TODO connect to peer even though we are full
            // ditch a peer ?

            // for now :
            try {
                the_node_handler.my_socket_.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
            return;
        }

        if (the_node_handler.the_server_.the_neighbor_nodes_.containsKey(the_sender_.GetTheData().the_guid_)
                || the_sender_.GetTheData().the_guid_.equals(the_node_handler.the_server_.my_node_.GetTheData().the_guid_)) {
            the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
            try {
                the_node_handler.my_socket_.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        the_node_handler.the_server_.the_neighbor_nodes_.put(the_sender_.GetTheData().the_guid_, the_node_handler);

        the_neighbors_.ActionOnReceive(the_node_handler);

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
    }

    @Override
    public void ActionOnSend(NodeConnectionHandler the_node_handler) {
        try {
            the_node_handler.my_socket_data_out_.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
