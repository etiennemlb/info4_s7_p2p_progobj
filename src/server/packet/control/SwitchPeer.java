package server.packet.control;

import java.io.IOException;

import server.Node;
import server.NodeConnectionHandler;
import server.packet.PacketBase;
import util.Utils;

public class SwitchPeer implements PacketBase {

    /**
     * 
     */
    private static final long serialVersionUID = -6531162585483177175L;

    protected final Node the_new_node_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // JoinNetwork implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public SwitchPeer(Node the_new_node) {
        the_new_node_ = the_new_node;
    }

    @Override
    public void ActionOnReceive(NodeConnectionHandler the_node_handler) {
        the_node_handler.the_server_.the_neighbor_nodes_mutex_.lock();
        the_node_handler.the_server_.the_neighbor_nodes_.remove(the_node_handler.my_destination_node_info_.GetTheData().the_guid_);
        the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();

        try {
            the_node_handler.my_socket_.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            new NodeConnectionHandler(the_node_handler.the_server_,
                                      the_new_node_.the_addr_,
                                      the_new_node_.the_port_).start();
        } catch (NumberFormatException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * make sure the_neighbor_nodes_mutex_ is locked prior to calling
     */
    @Override
    public void ActionOnSend(NodeConnectionHandler the_node_handler) {

        Utils.BetterAssert(the_node_handler.the_server_.the_neighbor_nodes_mutex_.isLocked());

        try {
            the_node_handler.my_socket_data_out_.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        the_node_handler.the_server_.the_neighbor_nodes_.remove(the_node_handler.my_destination_node_info_.GetTheData().the_guid_);

        try {
            the_node_handler.my_socket_.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
