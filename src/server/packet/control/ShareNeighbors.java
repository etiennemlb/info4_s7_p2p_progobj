package server.packet.control;

import java.io.IOException;
import java.util.Map;
import java.util.Vector;

import server.Node;
import server.NodeConnectionHandler;
import server.Server;
import server.packet.PacketBase;
import util.Utils;

public class ShareNeighbors implements PacketBase {

    /**
     * 
     */
    private static final long serialVersionUID = -251027627046597870L;

    protected final Vector<Node> the_neighbors_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // ShareNeighbors implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Be sure the_neighbor_nodes_mutex_ is locked
     * 
     * @param the_handler_to_exclude
     */
    public ShareNeighbors(NodeConnectionHandler the_handler_to_exclude) {
        the_neighbors_ = new Vector<Node>(5);

        Utils.BetterAssert(the_handler_to_exclude.the_server_.the_neighbor_nodes_mutex_.isLocked());

        for (Map.Entry<String, NodeConnectionHandler> an_entry : the_handler_to_exclude.the_server_.the_neighbor_nodes_.entrySet()) {
            if (an_entry.getValue().my_destination_node_info_ == /* no equals */ the_handler_to_exclude.my_destination_node_info_) {
                continue;
            }

            the_neighbors_.add(an_entry.getValue().my_destination_node_info_);
        }
    }

    /**
     * When a node receive this packet type, it needs to do the following :
     * 
     * - If thee server HasFreeNodeSlot, then try to establish new connections.
     * 
     * Be sure the_neighbor_nodes_mutex_ is locked !
     * 
     */
    @Override
    public void ActionOnReceive(NodeConnectionHandler the_node_handler) {

        Utils.BetterAssert(the_neighbors_.size() < Server.kMaxNeighborCount);
        Utils.BetterAssert(the_node_handler.the_server_.the_neighbor_nodes_mutex_.isLocked());

        for (Node a_node_info : the_neighbors_) {
            if (the_node_handler.the_server_.the_neighbor_nodes_.containsKey(a_node_info.GetTheData().the_guid_)) {
                continue;
            }

            if (!the_node_handler.the_server_.HasFreeNodeSlot()) {
                break;
            }

            try {
                // Let NodeConnectionHandler establish the connection, and if successful, add
                // itself to the server map.
                // Its possible that the NodeConnectionHandler wont be able to add itself to the
                // map in case an other thread is also trying to add itself to the map.
                new NodeConnectionHandler(the_node_handler.the_server_,
                                          a_node_info.the_addr_,
                                          a_node_info.the_port_).start();
            } catch (NumberFormatException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void ActionOnSend(NodeConnectionHandler the_node_handler) {

        try {
            the_node_handler.my_socket_data_out_.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
