package server.packet;

import java.io.Serializable;

import server.NodeConnectionHandler;

public interface PacketBase extends Serializable {

    public void ActionOnReceive(NodeConnectionHandler the_node_handler);

    public void ActionOnSend(NodeConnectionHandler the_node_handler);

}
