package server.packet.data;

import java.io.IOException;
import java.util.Map;

import server.Node;
import server.NodeConnectionHandler;
import server.Server;
import server.packet.PacketBase;

public class Query<T> implements PacketBase {

    /**
     * 
     */
    private static final long serialVersionUID = 2609824602606446629L;

    protected final String the_sender_guid_;
    protected final String the_destination_guid_;

    protected final OnQueryCallback<T> the_query_callback_;
    protected final OnDataCallback<T> the_data_callback_;

    protected int TTL_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // Query implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public Query(String the_sender_guid,
                 String the_destination,
                 OnQueryCallback<T> the_query_callback,
                 OnDataCallback<T> the_data_callback) {
        the_sender_guid_ = the_sender_guid;
        the_destination_guid_ = the_destination;

        the_query_callback_ = the_query_callback;
        the_data_callback_ = the_data_callback;

        TTL_ = Server.kTTL;
    }

    @Override
    public void ActionOnReceive(NodeConnectionHandler the_node_handler) {

        if (the_destination_guid_.equals(the_node_handler.the_server_.my_node_.GetTheData().the_guid_)) {
            new Response<T>(the_destination_guid_,
                            the_sender_guid_,
                            the_query_callback_.ProcessQuery(new Node(the_node_handler.the_server_.my_node_)),
                            the_data_callback_).ActionOnSend(the_node_handler);
            return;
        }

        // TODO Sort of Java CRTP to factorize the forwarding code (similar to the one in
        // Response), into a templated Forward method in the base class

        TTL_ = TTL_ - 1;
        if (TTL_ == 0) {
            // TODO
            // new Response<T>(the_destination_guid_,
            // the_sender_guid_,
            // /* new UnreachablePeerError() */,
            // the_data_callback_).ActionOnSend(the_node_handler);
            return; // Silent packet ditching for now
        }

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.lock();

        // Shortcut, we know this destination
        if (the_node_handler.the_server_.the_neighbor_nodes_.containsKey(the_destination_guid_)) {
            ActionOnSend(the_node_handler.the_server_.the_neighbor_nodes_.get(the_destination_guid_));
            return;
        }

        for (Map.Entry<String, NodeConnectionHandler> an_entry : the_node_handler.the_server_.the_neighbor_nodes_.entrySet()) {

            if (an_entry.getValue().my_destination_node_info_ == /* no equals */ the_node_handler.my_destination_node_info_) {
                continue; // Dont send it to the sender
            }

            ActionOnSend(an_entry.getValue());
        }

        the_node_handler.the_server_.the_neighbor_nodes_mutex_.unlock();
    }

    @Override
    public void ActionOnSend(NodeConnectionHandler the_node_handler) {
        try {
            the_node_handler.my_socket_data_out_.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
