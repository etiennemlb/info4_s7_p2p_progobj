package server.packet.data;

import java.io.Serializable;

import server.Node;

public interface OnQueryCallback<T> extends Serializable {
    public T ProcessQuery(Node the_node);
}
