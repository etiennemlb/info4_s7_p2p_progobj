package server.packet.data;

import java.io.Serializable;

public interface OnDataCallback<T> extends Serializable {
    public void ProcessData(T the_object);
}
