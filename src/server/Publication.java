package server;

import java.io.Serializable;
import java.util.Date;

public class Publication implements Serializable {

    private static final long serialVersionUID = -4317454257867941471L;

    public String the_publication_text_;
    public Date the_publication_date_;

    public Publication(Publication a_publication) {
        the_publication_text_ = a_publication.the_publication_text_;
        the_publication_date_ = (Date) a_publication.the_publication_date_.clone();
    }

    public Publication(String the_publication_text) {
        the_publication_text_ = the_publication_text;
        the_publication_date_ = new Date(); // Time of creation
    }

    @Override
    public String toString() {
        return "Publication [the_publication_text_=" + the_publication_text_ + ", the_publication_date_="
                + the_publication_date_ + "]";
    }

};