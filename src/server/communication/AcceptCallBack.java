package server.communication;

import java.net.Socket;

public interface AcceptCallback {
    public void ProcessNewConnection(Socket the_socket);
}
