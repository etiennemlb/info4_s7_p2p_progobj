package server.communication;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketListener extends Thread {

    private final int kBackLog = 15;

    protected ServerSocket the_listening_socket_;
    protected AcceptCallback the_callback_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // SocketListener implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public boolean Init(AcceptCallback the_callback, String the_local_ip, String the_port) throws IOException {
        the_callback_ = the_callback;

        the_listening_socket_ = new ServerSocket();
        the_listening_socket_.setReuseAddress(true);
        the_listening_socket_.bind(new InetSocketAddress(InetAddress.getByName(the_local_ip),
                                                         Short.parseShort(the_port)),
                                   kBackLog);

        if (the_listening_socket_.isClosed()) {
            return false;
        }

        this/* base */.start();

        return true;
    }

    public void run() {
        try {
            while (!the_listening_socket_.isClosed()) {

                Socket the_new_socket = the_listening_socket_.accept();
                the_callback_.ProcessNewConnection(the_new_socket); // Maybe this callback should be run on an other
                                                                    // thread

            }
        } catch (Exception e) {
            // Whe the socket is listening and Close() is called, accept() will throw
            // SocketException
        } finally {
            // Close (again potentially), accept / ProcessNewConnection encounter some kind
            // of error
            Close();
        }
    }

    public void Close() {
        try {
            the_listening_socket_.close();
        } catch (IOException e) {
        }
    }
}
