package server;

import java.io.Serializable;
import java.util.Vector;

public class PerishableNodeData implements Serializable {

    private static final long serialVersionUID = 8782924659455566227L;

    public String the_status_;
    public Vector<Publication> the_past_publications_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // Node PerishableNodeData
    //////////////////////////////////////////////////////////////////////////////////////////

    public PerishableNodeData(PerishableNodeData a_perishable_node_data) {
        this(a_perishable_node_data.the_status_);

        for (Publication a_publication : a_perishable_node_data.the_past_publications_) {
            the_past_publications_.add(new Publication(a_publication));
        }
    }

    public PerishableNodeData() {
        the_status_ = new String();
        the_past_publications_ = new Vector<>();
    }

    public PerishableNodeData(String the_status) {
        the_status_ = the_status;
        the_past_publications_ = new Vector<>();
    }

    @Override
    public String toString() {
        return "PerishableNodeData [the_status_=" + the_status_ + ", the_past_publications_=" + the_past_publications_
                + "]";
    }

}
