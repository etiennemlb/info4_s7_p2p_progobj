package server;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.locks.ReentrantLock;

import server.communication.AcceptCallback;
import server.communication.SocketListener;
import server.packet.data.OnDataCallback;
import server.packet.data.OnQueryCallback;
import server.packet.data.Query;
import server.packet.control.LeavingNetwork;
import util.Utils;

/**
 * Chaque étudiant de Polytech (appelé ensuite un "utilisateur") peut faire
 * tourner sur son PC personnel une instance du logiciel. Cette instance est
 * appelée un "noeud", l'ensemble des noeuds est appelé le "système".
 * 
 * Chaque instance hébèrge des informations sur son utilisateur : nom, prénom,
 * identifiant unique, statut (une phrase courte) et publications (textes avec
 * une date).
 * 
 * Chaque noeud joue le rôle de serveur : il est en écoute sur le réseau (à une
 * adresse IP donc, et sur un port donné) et peut répondre aux requêtes
 * concernant son utilisateur (récupération des informations demandées, voir
 * plus loin). [Dans la version simulée, le service est rendu par appel de
 * méthode.]
 * 
 * Chaque noeud peut mémoriser les informations pour contacter AU PLUS 5 autres
 * noeuds : adresse IP et port [ou référence de l'objet pour la version simulée]
 * et utilisateur sur ce noeud. Ces noeuds dont on a mémorisé les informations
 * sont appelés des "voisins".
 * 
 * Chaque utilisateur peut demander au système, via son noeud, des informations
 * de n'importe quel autre utilisateur : nom, prénom, identifiant, statut,
 * dernières publications.
 * 
 * Si le noeud d'un utilisateur ne sait pas contacter le noeud de l'utilisateur
 * demandé, il transfère sa requête aux voisins (parmi les 5 mémorisés) qui
 * eux-mêmes peuvent la faire suivre, et ainsi de suite.
 * 
 * Chaque utilisateur peut modifier son statut à tout moment, ainsi qu'ajouter
 * une publication.
 * 
 * (not fully supported. This functionality cost us one hop more than it
 * should according to the following spec) -> Si un utilisateur A cherche une
 * information sur un utilisateur B mais qu'il s'adresse à C pour obtenir cette
 * information, alors C n'est pas obligé de router la demande jusqu'à B si il
 * détient déjà cette information. En particulier, les noms, prénoms et
 * identifiants uniques sont intemporels, contrairement aux statuts et aux
 * publications. Attention, on ne peut mémoriser des informations que sur ses 5
 * voisins.
 * 
 * NOT SUPPORTED (If a user leaves the network (properly or not), it's not
 * reachable/queryable) -> Si un utilisateur A cherche une information
 * périssable sur un utilisateur B (statut, publications), et que B n'est pas
 * joignable (son système est éteint, ou n'est pas accessible par le réseau, ou
 * personne de joignable ne connaît son adresse), alors n'importe qui qui a des
 * données sur B en mémoire peut donner ces informations, en précisant qu'elles
 * proviennent d'un cache. (À nouveau, on ne peut mémoriser des informations que
 * sur ses 5 voisins.)
 * 
 * [FACULTATIF] Chaque utilisateur peut suivre d'autres utilisateurs (comme sur
 * Twitter). Un utilisateur peut demander à son système de lui montrer les
 * derniers statuts et les dernières publications de tous les utilisateurs qu'il
 * suit. On peut suivre plus de 5 utilisateurs (mais on ne peut toujours pas
 * mémoriser les informations de plus de 5 utilisateurs).
 * 
 * Un utilisateur peut entrer manuellement dans son système l'adresse d'un autre
 * utilisateur. Ainsi si quelqu'un s'apperçoit qu'il est injoignable car
 * personne n'a son adresse, il peut demander informellement à un camarade de le
 * référencer. Attention, l'ajout manuel ne permet pas de dépasser la limite de
 * 5 voisins (on doit effacer un voisin si on en avait déjà 5).
 * 
 * [FACULTATIF] Une fois qu'un noeud a un voisin, ce noeud peut demander à ce
 * voisin de lui communiquer d'autres adresses afin de remplir sa table de
 * voisins.
 * 
 * /!\UI (not perfectly supported, we are not able to force peers to connect
 * to us at a given point in time. So it is possible but subject to randomness)
 * -> [FACULTATIF pour la version réseau] Lorsqu'un noeud s'éteint proprement
 * (sur demande de l'utilisateur), il sauvegarde les informations de son
 * utilisateur et la liste de ses voisins, qu'il rechargera au prochain
 * lancement.
 *
 */
public class Server {

    public static final int kMaxNeighborCount = 5;
    public static final int kTTL = 5;

    /**
     * The data of the current node running this server instance.
     */
    public final Node my_node_;

    /**
     * Key, guid, value, the Node handler
     */
    public final HashMap<String, NodeConnectionHandler> the_neighbor_nodes_;

    public final ReentrantLock the_neighbor_nodes_mutex_;

    protected final SocketListener the_listening_socket_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // Server implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public Server(Node my_node) {
        my_node_ = my_node;
        the_neighbor_nodes_ = new HashMap<>(kMaxNeighborCount);
        the_neighbor_nodes_mutex_ = new ReentrantLock();

        the_listening_socket_ = new SocketListener();
    }

    public boolean Start() throws IOException {
        AcceptCallback the_accept_callback = new AcceptCallback() {
            @Override
            public void ProcessNewConnection(Socket the_socket) {
                try {
                    new NodeConnectionHandler(Server.this, the_socket, false).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Utils.Log("Initialising the server, listening on " + my_node_.the_addr_ + ':' + my_node_.the_port_);

        return the_listening_socket_.Init(the_accept_callback, my_node_.the_addr_, my_node_.the_port_);
    }

    public boolean Join(String the_addr, String the_port) {
        try {
            Utils.Log("Starting a new NodeConnectionHandler, connecting to " + the_addr + ':' + the_port);
            new NodeConnectionHandler(this, the_addr, the_port).start();
        } catch (NumberFormatException | IOException e) {
            // e.printStackTrace();
            return false;
        }
        return true;
    }

    public <T> void QueryNode(String the_destination_guid,
                              OnDataCallback<T> the_callback_on_data,
                              OnQueryCallback<T> the_callback_on_query) {

        the_neighbor_nodes_mutex_.lock();

        for (Map.Entry<String, NodeConnectionHandler> an_entry : the_neighbor_nodes_.entrySet()) {
            new Query<T>(my_node_.GetTheData().the_guid_,
                         the_destination_guid,
                         the_callback_on_query,
                         the_callback_on_data).ActionOnSend(an_entry.getValue());
        }

        the_neighbor_nodes_mutex_.unlock();
    }

    /**
     * To avoid bad surprises, lock the_neighbor_nodes_mutex_ before calling.
     * 
     * @return true if it HasFreeNodeSlot.
     */
    public boolean HasFreeNodeSlot() {
        Utils.BetterAssert(the_neighbor_nodes_mutex_.isLocked()); // ~~
        return the_neighbor_nodes_.size() < kMaxNeighborCount;
    }

    /**
     * Cleanly leave the network (notify the neighbors).
     */
    public void Stop() {

        Utils.Log("Closing the server, stopping listening");
        the_listening_socket_.Close();

        // Notify neighbors
        the_neighbor_nodes_mutex_.lock();

        Vector<NodeConnectionHandler> the_neighbors = new Vector<>(the_neighbor_nodes_.size());

        for (Map.Entry<String, NodeConnectionHandler> an_entry : the_neighbor_nodes_.entrySet()) {
            the_neighbors.add(an_entry.getValue());
        }

        Utils.Log("Leaving the network, notifying the neighbors");
        for (NodeConnectionHandler a_node : the_neighbors) {
            new LeavingNetwork(a_node).ActionOnSend(a_node);
        }

        the_neighbor_nodes_mutex_.unlock();
    }

    @Override
    public String toString() {
        the_neighbor_nodes_mutex_.lock();
        String the_string = "Server [my_node_=" + my_node_ + ",\nthe_neighbor_nodes_=" + the_neighbor_nodes_ + "]";
        the_neighbor_nodes_mutex_.unlock();
        return the_string;
    }

}
