package server;

import java.io.Serializable;

public class Node implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6815401512560628485L;

    /**
     * This node listen on this ip
     */
    public final String the_addr_;
    /**
     * This node listen on this port
     */
    public final String the_port_;

    protected final NodeData the_data_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // Node implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public Node(Node a_node) {
        this(a_node.the_addr_, a_node.the_port_, new NodeData(a_node.the_data_));
    }

    public Node(String the_addr, String the_port, String the_guid) {
        this(the_addr, the_port, new NodeData(the_guid));
    }

    public Node(String the_addr, String the_port, NodeData the_node_data) {
        the_addr_ = the_addr;
        the_port_ = the_port;
        the_data_ = the_node_data;
    }

    public NodeData GetTheData() {
        return the_data_;
    }

    @Override
    public String toString() {
        return "\n\tNode [the_addr_=" + the_addr_ + ", the_port_=" + the_port_ + ", the_data_=" + the_data_ + "]";
    }

}
