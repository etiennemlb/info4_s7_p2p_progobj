package server;

import java.io.Serializable;

public class NodeData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5323978132213910486L;

    public final String the_name_;
    public final String the_surname_;
    public final String the_guid_;

    public final PerishableNodeData the_perishable_data_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // NodeData implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public NodeData(NodeData a_node_data) {
        this(a_node_data.the_name_,
             a_node_data.the_surname_,
             a_node_data.the_guid_,
             new PerishableNodeData(a_node_data.the_perishable_data_));
    }

    public NodeData(String the_guid) {
        the_name_ = new String();
        the_surname_ = new String();
        the_guid_ = the_guid;
        the_perishable_data_ = new PerishableNodeData();
    }

    public NodeData(String the_name, String the_surname, String the_guid, PerishableNodeData the_perishable_data) {
        the_name_ = the_name;
        the_surname_ = the_surname;
        the_guid_ = the_guid;
        the_perishable_data_ = the_perishable_data;
    }

    @Override
    public String toString() {
        return "NodeData [the_name_=" + the_name_ + ", the_surname_=" + the_surname_ + ", the_guid_=" + the_guid_
                + ", the_perishable_data_=" + the_perishable_data_ + "]";
    }

}
