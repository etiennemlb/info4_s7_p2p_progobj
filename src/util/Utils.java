package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {

    enum LogType {
        kError, kMsg
    };

    public static void Log(String the_message) {
        Log(LogType.kMsg, the_message);
    }

    public static void Log(LogType the_log_type, String the_message) {
        StringBuilder the_string = new StringBuilder();

        if (the_log_type == LogType.kMsg) {
            the_string.append("MSG:");
        } else if (the_log_type == LogType.kError) {
            the_string.append("ERR:");
        }

        the_string.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
        the_string.append(':');
        the_string.append(Thread.currentThread().getName());
        the_string.append('#');
        the_string.append(Thread.currentThread().getId());
        the_string.append(':');
        the_string.append(the_message);

        if (the_log_type == LogType.kMsg) {
            System.out.println(the_string.toString());
        } else if (the_log_type == LogType.kError) {
            System.err.println(the_string.toString());
        }
    }

    public static void BetterAssert(boolean the_condition) {
        if (!the_condition) {
            StackTraceElement[] the_stacktrace = Thread.currentThread().getStackTrace();
            // for(int i = 0; i < the_stacktrace.length; ++i) {
            // System.out.println(the_stacktrace[i]);
            // }
            System.err.println("Erreur d'assertion dans " + the_stacktrace[2].getFileName() + ':'
                    + the_stacktrace[2].getLineNumber()); // On remonte de 2 étages, // <- java good ?
        }
    }
}
