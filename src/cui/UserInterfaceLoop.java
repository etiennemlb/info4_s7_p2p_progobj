package cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map.Entry;

import cui.actions.AddPublication;
import cui.actions.CloseServer;
import cui.actions.Connect;
import cui.actions.LoadPeersFromDisk;
import cui.actions.QueryPerishable;
import cui.actions.QueryStatusOfFollowedPeers;
import cui.actions.AddFollow;
import cui.actions.ChangeStatus;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.PresentBanner;
import server.Node;
import server.NodeData;
import server.PerishableNodeData;
import server.Server;

public class UserInterfaceLoop {

    public final static String kTheOldPeersFile = "old_peers.bak";

    /**
     * Contains all the user accessible actions (not PresentBanner and
     * QueryUserAction for instance)
     */
    private HashMap<String, Action> the_registered_user_actions_;

    protected Action the_current_action_;
    protected Server the_server_;

    //////////////////////////////////////////////////////////////////////////////////////////
    // UserInterfaceLoop implementation
    //////////////////////////////////////////////////////////////////////////////////////////

    public UserInterfaceLoop(String the_ip,
                             String the_port,
                             String the_name,
                             String the_surname,
                             String the_guid,
                             PerishableNodeData the_perishable_data) throws IOException {

        the_registered_user_actions_ = new HashMap<>();
        PopulateUserAction();

        the_current_action_ = new PresentBanner().DoAction(this);

        the_server_ = new Server(new Node(the_ip,
                                          the_port,
                                          new NodeData(the_name, the_surname, the_guid, the_perishable_data)));

    }

    private void PopulateUserAction() {
        AddUserAction(new QueryPerishable());
        AddUserAction(new Connect());
        AddUserAction(new LoadPeersFromDisk());
        AddUserAction(new CloseServer());
        AddUserAction(new ChangeStatus());
        AddUserAction(new AddFollow());
        AddUserAction(new AddPublication());
        AddUserAction(new QueryStatusOfFollowedPeers());
    }

    private void AddUserAction(Action the_action) {
        if (the_registered_user_actions_.containsKey(the_action.GetActionTriggerCharacterSequence())) {
            throw new RuntimeException("Similar action code is not allowed !");
        }
        the_registered_user_actions_.put(the_action.GetActionTriggerCharacterSequence(), the_action);
    }

    public Action GetUserAction(String the_action_code) {

        if (!the_registered_user_actions_.containsKey(the_action_code)) {
            return null;
        }

        return the_registered_user_actions_.get(the_action_code);
    }

    public void PrintAvailableActions() {
        StringBuilder the_str = new StringBuilder();

        the_str.append("The user available actions are:\n");

        for (Entry<String, Action> an_entry : the_registered_user_actions_.entrySet()) {
            the_str.append('\t');
            the_str.append("Use ");
            the_str.append(an_entry.getKey());
            the_str.append("\tto: ");
            the_str.append(an_entry.getValue().GetActionDesc());
            the_str.append('\n');
        }

        System.out.println(the_str.toString());

    }

    public void Run() {
        try {
            the_server_.Start();

            for (;;) {
                the_current_action_ = the_current_action_.DoAction(this);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Server GetServer() {
        return the_server_;
    }

    public static void main(String[] the_args) {
        try {

            System.out.println("Before launching the server, some question:");

            System.out.print("On which ip/interface do you wish to listen (noting is localhost)?:");
            String our_host = new BufferedReader(new InputStreamReader(System.in)).readLine();
            our_host = our_host.equals("") ? "localhost" : our_host;

            System.out.print("Using which port?:");
            String our_port = new BufferedReader(new InputStreamReader(System.in)).readLine();

            System.out.print("What is your guid (unique identifier, eg, E156547G, entering nothing will set the port as guid)?:");
            String our_guid = new BufferedReader(new InputStreamReader(System.in)).readLine();
            our_guid = our_guid.equals("") ? our_port : our_guid;

            System.out.print("What is your name (eg, Etienne)?:");
            String our_name = new BufferedReader(new InputStreamReader(System.in)).readLine();

            System.out.print("What is your surname (eg, Malaboeuf)?:");
            String our_surname = new BufferedReader(new InputStreamReader(System.in)).readLine();

            UserInterfaceLoop the_cui = new UserInterfaceLoop(our_host,
                                                              our_port,
                                                              our_name,
                                                              our_surname,
                                                              our_guid,
                                                              new PerishableNodeData("Freshly connected !"));

            the_cui.Run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
