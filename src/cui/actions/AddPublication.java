package cui.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cui.UserInterfaceLoop;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;
import server.Publication;

public class AddPublication implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {
        try {
            System.out.print("Enter an new publication (one line):");
            String the_new_publication = new BufferedReader(new InputStreamReader(System.in)).readLine();

            the_user_interface.GetServer().my_node_.GetTheData().the_perishable_data_.the_past_publications_.add(new Publication(the_new_publication));

            System.out.println("New publication added !");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new QueryUserAction();// Go back to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "ap";
    }

    @Override
    public String GetActionDesc() {
        return "Add a publication for others to see";
    }

}
