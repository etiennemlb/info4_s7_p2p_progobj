package cui.actions.non_user_available;

import cui.UserInterfaceLoop;

public interface Action {

    public Action DoAction(UserInterfaceLoop the_user_interface);

    /**
     * A shame we can't realy use static methods ("this" dont work)
     * 
     * @return
     */
    public String GetActionTriggerCharacterSequence();

    /**
     * A shame we can't realy use static methods ("this" dont work)
     * 
     * @return
     */
    public String GetActionDesc();
}
