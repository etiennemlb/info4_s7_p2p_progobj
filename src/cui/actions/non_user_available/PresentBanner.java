package cui.actions.non_user_available;

import cui.UserInterfaceLoop;

public class PresentBanner implements Action {

    protected void PrintBanner() {
        System.out.println("\n" + "   ▄▀▀▄    ▄▀▀▄  ▄▀▀█▄▄▄▄  ▄▀▀▀▀▄     ▄▀▄▄▄▄   ▄▀▀▀▀▄   ▄▀▀▄ ▄▀▄  ▄▀▀█▄▄▄▄ \r\n"
                + "  █   █    ▐  █ ▐  ▄▀   ▐ █    █     █ █    ▌ █      █ █  █ ▀  █ ▐  ▄▀   ▐ \r\n"
                + "  ▐  █        █   █▄▄▄▄▄  ▐    █     ▐ █      █      █ ▐  █    █   █▄▄▄▄▄  \r\n"
                + "    █   ▄    █    █    ▌      █        █      ▀▄    ▄▀   █    █    █    ▌  \r\n"
                + "     ▀▄▀ ▀▄ ▄▀   ▄▀▄▄▄▄     ▄▀▄▄▄▄▄▄▀ ▄▀▄▄▄▄▀   ▀▀▀▀   ▄▀   ▄▀    ▄▀▄▄▄▄   \r\n"
                + "           ▀     █    ▐     █        █     ▐           █    █     █    ▐   \r\n"
                + "                 ▐          ▐        ▐                 ▐    ▐     ▐     \r\n");
    }

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {
        PrintBanner();
        return new QueryUserAction(); // Go to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return null;// UNIMPLEMENTED
    }

    @Override
    public String GetActionDesc() {
        return null;// UNIMPLEMENTED
    }

}
