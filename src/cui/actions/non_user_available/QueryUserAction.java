package cui.actions.non_user_available;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import cui.UserInterfaceLoop;

public class QueryUserAction implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {
        the_user_interface.PrintAvailableActions();

        for (;;) {

            try {
                System.out.print("Select a command:");
                String the_line = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();

                Action the_next_action = the_user_interface.GetUserAction(the_line);

                if (the_next_action == null) {
                    System.out.println("Unknown command, retry please...");
                    continue;
                }

                return the_next_action;

            } catch (IOException e) {
            }
        }
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return null; // UNIMPLEMENTED
    }

    @Override
    public String GetActionDesc() {
        return null;// UNIMPLEMENTED
    }

}
