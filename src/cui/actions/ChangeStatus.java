package cui.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cui.UserInterfaceLoop;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;

public class ChangeStatus implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {

        try {
            System.out.print("Enter an new status:");
            String the_new_status = new BufferedReader(new InputStreamReader(System.in)).readLine();

            the_user_interface.GetServer().my_node_.GetTheData().the_perishable_data_.the_status_ = the_new_status;

            System.out.println("Status changed to:'" + the_new_status + "'!");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new QueryUserAction();// Go back to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "cs";
    }

    @Override
    public String GetActionDesc() {
        return "Change the perishable data (status mainly)";
    }

}
