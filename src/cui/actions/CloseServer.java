package cui.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Map;

import cui.UserInterfaceLoop;
import cui.actions.non_user_available.Action;
import server.NodeConnectionHandler;
import util.Utils;

public class CloseServer implements Action {

    @SuppressWarnings("deprecation")
    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {

        try {
            File a_file = new File(UserInterfaceLoop.kTheOldPeersFile);

            ObjectOutputStream the_serializer = new ObjectOutputStream(new FileOutputStream(a_file));

            the_user_interface.GetServer().the_neighbor_nodes_mutex_.lock();
            int the_peer_count = the_user_interface.GetServer().the_neighbor_nodes_.size();

            the_serializer.writeObject(new Integer(the_peer_count));

            for (Map.Entry<String, NodeConnectionHandler> an_entry : the_user_interface.GetServer().the_neighbor_nodes_.entrySet()) {
                the_serializer.writeObject(an_entry.getValue().my_destination_node_info_.the_addr_);
                the_serializer.writeObject(an_entry.getValue().my_destination_node_info_.the_port_);
            }

            the_user_interface.GetServer().the_neighbor_nodes_mutex_.unlock();
            the_serializer.close();
        } catch (IOException e) {
            e.printStackTrace();
            the_user_interface.GetServer().the_neighbor_nodes_mutex_.unlock();
        }

        the_user_interface.GetServer().Stop();

        Utils.Log("The server should be stoped, exiting...");

        System.exit(0);
        return null; // Never reached
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "cl";
    }

    @Override
    public String GetActionDesc() {
        return "Closes the server";
    }
}
