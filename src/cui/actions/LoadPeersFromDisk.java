package cui.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import cui.UserInterfaceLoop;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;

public class LoadPeersFromDisk implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {

        try {
            File a_file = new File(UserInterfaceLoop.kTheOldPeersFile);

            ObjectInputStream the_deserializer = new ObjectInputStream(new FileInputStream(a_file));

            int the_peer_count = (int) the_deserializer.readObject();

            for (int i = 0; i < the_peer_count; ++i) {
                String the_host = (String) the_deserializer.readObject();
                String the_port = (String) the_deserializer.readObject();
                the_user_interface.GetServer().Join(the_host, the_port);
            }

            the_deserializer.close();
        } catch (FileNotFoundException e) {
            System.out.println("No old peer file !");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return new QueryUserAction();// Go back to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "l";
    }

    @Override
    public String GetActionDesc() {
        return "Try to connect to the peers of an older session";
    }

}
