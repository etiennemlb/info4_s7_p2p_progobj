package cui.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cui.UserInterfaceLoop;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;

public class AddFollow implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {

        QueryStatusOfFollowedPeers the_follower_query_action = (QueryStatusOfFollowedPeers) the_user_interface.GetUserAction(/*
                                                                                                                              * new
                                                                                                                              * because
                                                                                                                              * static
                                                                                                                              * method
                                                                                                                              * is
                                                                                                                              * not
                                                                                                                              * allowed
                                                                                                                              * in
                                                                                                                              * an
                                                                                                                              * interface
                                                                                                                              */new QueryStatusOfFollowedPeers().GetActionTriggerCharacterSequence());

        if (the_follower_query_action == null) {
            throw new RuntimeException("Action unavailable!");
        }

        System.out.print("Enter the guid of the peer to follow please:");

        try {
            String the_new_guid_to_follow = new BufferedReader(new InputStreamReader(System.in)).readLine();
            the_follower_query_action.AddGuidToFollow(the_new_guid_to_follow);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("The guid has been added to the list of followed guid !");

        return new QueryUserAction();// Go back to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "f";
    }

    @Override
    public String GetActionDesc() {
        return "Manage your followed peers";
    }

}
