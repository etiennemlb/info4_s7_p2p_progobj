package cui.actions;

import java.util.HashSet;

import cui.UserInterfaceLoop;
import cui.actions.callbacks.CbOnPerishableData;
import cui.actions.callbacks.CbOnPerishableDataQuery;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;

public class QueryStatusOfFollowedPeers implements Action {

    protected HashSet<String> the_followed_guid_;

    public QueryStatusOfFollowedPeers() {
        the_followed_guid_ = new HashSet<>();
    }

    public void AddGuidToFollow(String the_guid) {
        the_followed_guid_.add(the_guid);
    }

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {

        for (String a_guid : the_followed_guid_) {
            the_user_interface.GetServer()
                              .QueryNode(a_guid,
                                         new CbOnPerishableData(a_guid),
                                         new CbOnPerishableDataQuery());
        }

        return new QueryUserAction();
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "qf";
    }

    @Override
    public String GetActionDesc() {
        return "Query all the followed peer for theirs status and publication";
    }

}
