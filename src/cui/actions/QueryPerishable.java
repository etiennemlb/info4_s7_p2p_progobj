package cui.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cui.UserInterfaceLoop;
import cui.actions.callbacks.CbOnPerishableData;
import cui.actions.callbacks.CbOnPerishableDataQuery;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;

public class QueryPerishable implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {

        try {
            System.out.print("Enter a guid representing a potentially connected node:");

            String the_destination_guid = new BufferedReader(new InputStreamReader(System.in)).readLine();

            the_user_interface.GetServer()
                              .QueryNode(the_destination_guid,
                                         new CbOnPerishableData(the_destination_guid),
                                         new CbOnPerishableDataQuery());

            System.out.println("The query was sent. Because you are using a fully P2P network, you may never have an answer !");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new QueryUserAction();// Go back to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "qs";
    }

    @Override
    public String GetActionDesc() {
        return "Query the network for information about an user's perishable data";
    }
};
