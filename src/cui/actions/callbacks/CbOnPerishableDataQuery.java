package cui.actions.callbacks;

import server.Node;
import server.PerishableNodeData;
import server.packet.data.OnQueryCallback;

/**
 * This class needs to be out of QueryPerishable for serialization purpose.
 * 
 * @author Etienne
 *
 */
public final class CbOnPerishableDataQuery implements OnQueryCallback<PerishableNodeData> {

    private static final long serialVersionUID = 2133003422219275752L; // important

    public PerishableNodeData ProcessQuery(Node the_node) {
        return the_node.GetTheData().the_perishable_data_;
    }
};
