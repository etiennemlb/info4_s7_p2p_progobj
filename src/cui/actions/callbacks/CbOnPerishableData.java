package cui.actions.callbacks;

import javax.swing.JOptionPane;

import server.PerishableNodeData;
import server.packet.data.OnDataCallback;

/**
 * This class needs to be out of QueryPerishable for serialization purpose.
 * 
 * @author Etienne
 *
 */
public final class CbOnPerishableData implements OnDataCallback<PerishableNodeData> {

    private static final long serialVersionUID = 357365404470986264L; // important

    private final String the_destination_guid_;

    public CbOnPerishableData(String the_destination_guid) {
        the_destination_guid_ = the_destination_guid;
    }

    public void ProcessData(PerishableNodeData the_node_info) {
        try {
            JOptionPane.showMessageDialog(null,
                                          the_node_info.toString(),
                                          "We got a result from a perishable data query with the destination guid:"
                                                  + the_destination_guid_,
                                          JOptionPane.INFORMATION_MESSAGE);
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

};