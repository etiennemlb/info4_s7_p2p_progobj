package cui.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cui.UserInterfaceLoop;
import cui.actions.non_user_available.Action;
import cui.actions.non_user_available.QueryUserAction;

public class Connect implements Action {

    @Override
    public Action DoAction(UserInterfaceLoop the_user_interface) {
        try {
            System.out.print("Enter an ip please:");

            String the_ip = new BufferedReader(new InputStreamReader(System.in)).readLine();

            System.out.print("Enter a port please:");

            String the_port = new BufferedReader(new InputStreamReader(System.in)).readLine();

            if (!the_user_interface.GetServer().Join(the_ip, the_port)) {
                System.out.println("An error occured, the connection will not be established !");
            } else {
                System.out.println("The server asked for a connection. No message will "
                        + "be displayed if the connection is successful or has failed.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new QueryUserAction();// Go back to the selection menu
    }

    @Override
    public String GetActionTriggerCharacterSequence() {
        return "co";
    }

    @Override
    public String GetActionDesc() {
        return "Manually connect to a peer";
    }

}
